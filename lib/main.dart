import 'package:bloc_counter/bloc/counter_bloc.dart';
import 'package:bloc_counter/bloc/counter_event.dart';
import 'package:bloc_counter/counter_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocProvider(
          create: (BuildContext ctx) => CounterBloc(),
          child: HomePage()),
    );
  }
}

class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    CounterBloc _counterBloc = BlocProvider.of<CounterBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('AppTitle'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          
          StreamBuilder(
            stream: _counterBloc ,
              builder: (context, snapshot){
            return Text(
              '${_counterBloc.state}',
            );
          }),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              CounterButton(
                iconData: Icons.add,
                onTap: () => _counterBloc.add(CounterEvent.incrementEvent),
              ),

              CounterButton(
                iconData: Icons.remove,
                onTap: () => _counterBloc.add(CounterEvent.decrementEvent),
              ),

              CounterButton(
                iconData: Icons.clear,
                onTap: () => _counterBloc.add(CounterEvent.clearEvent),
              ),
            ],
          )
        ],
      ),
    );
  }
}


