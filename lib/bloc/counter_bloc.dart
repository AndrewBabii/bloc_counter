
import 'package:bloc_counter/bloc/counter_event.dart';
import 'package:flutter_bloc/flutter_bloc.dart';



class CounterBloc extends Bloc<CounterEvent, int> {

  int _counter = 0;

  @override
  Stream<int> mapEventToState(CounterEvent event) async*{
    switch(event){
      case CounterEvent.incrementEvent :
        _counter++;
        break;
      case CounterEvent.decrementEvent :
        _counter--;
        break;
      case CounterEvent.clearEvent :
        _counter = 0;
    }
    yield _counter;
  }

  CounterBloc() : super(0);
}