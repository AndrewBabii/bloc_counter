enum CounterEvent {
  incrementEvent,
  decrementEvent,
  clearEvent,
}